#!/bin/bash

while true; do

/home/pi/opc_data.sh &
opc_pid=$!

offline="7"
led1="/home/pi/gpio/led1.py"
led2="/home/pi/gpio/led2.py"
led3="/home/pi/gpio/led3.py"
led4="/home/pi/gpio/led4.py"
gridoff="/home/pi/gpio/grid-off.py"
gridon="/home/pi/gpio/grid-on.py"
wind=`cat /var/www/html/wind.txt`
nuke=`cat /var/www/html/nuke.txt`
water=`cat /var/www/html/water.txt`
windStatus="/home/scadacs/wind.status"
nukeStatus="/home/scadacs/nuke.status"
waterStatus="/home/scadacs/water.status"

windStat=`cat $windStatus`
nukeStat=`cat $nukeStatus`
waterStat=`cat $waterStatus`

echo "$windStat" > /home/pi/wind.status
echo "$nukeStat" > /home/pi/nuke.status
echo "$waterStat" > /home/pi/water.status

if [ "$windStat" = "0" ]; then
  python $led2 red-on
elif [ "$wind" -lt "100" ] || [ "$wind" -gt "450" ]; then
  python $led2 red-on
  if [ -z "$iWind" ]; then
    iWind="1"
  else
    ((iWind++))
  fi
elif [ "$wind" -lt "150" ] || [ "$wind" -gt "400" ]; then
  python $led2 yellow-on
  iWind="1"
else
  python $led2 green-on
  iWind="1"
fi

if [ "$nukeStat" = "0" ]; then
  python $led3 red-on
elif [ "$nuke" -gt "3450" ] || [ "$nuke" -lt "3050" ]; then
  python $led3 red-on
  if [ -z "$iNuke" ]; then
    iNuke="1"
  else
    ((iNuke++))
  fi
elif [ "$nuke" -gt "3350" ] || [ "$nuke" -lt "3100" ]; then
  python $led3 yellow-on
  iNuke="1"
else
  python $led3 green-on
  iNuke="1"
fi

if [ "$waterStat" = "0" ]; then
  python $led4 red-on
elif [ "$water" -lt "-50" ] || [ "$water" -gt "50" ]; then
  python $led4 red-on
  if [ -z "$iWater" ]; then
    iWater="1"
  else
    ((iWater++))
  fi
elif [ "$water" -lt "-30" ] || [ "$water" -gt "30" ]; then
  python $led4 yellow-on
else
  python $led4 green-on
fi

if [ "$iWind" -gt "$offline" ]; then
  rm /home/scadacs/.wind.run
  echo "0" > $windStatus
fi

if [ "$iNuke" -gt "$offline" ]; then
  rm /home/scadacs/.nuke.run
  echo "0" > $nukeStatus
fi

if [ "$waterStat" -gt "$offline" ]; then
  rm /home/scadacs/.water.run
  echo "0" > $waterStatus
fi

if [ "$windStat" = "0" ] && [ "$nukeStat" = "0" ] && [ "$waterStat" = "0" ]; then
  python $led1 red-on
  python $gridoff
elif [ "$windStat" = "0" ] || [ "$nukeStat" = "0" ] || [ "$waterStat" = "0" ]; then
  python $led1 yellow-on
  python $gridon
else
  python $led1 green-on
  python $gridon
fi

sleep 10

ftp_procs=`ps -eaf | grep "ftp -in" | grep -v grep | awk '{print $2}'`
for p in ${ftp_procs[@]}
do
  kill $p
done

kill $opc_pid

done

exit
