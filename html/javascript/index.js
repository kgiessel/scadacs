window.onload = function(){

    var WindGuage = new GaugeSVG({
        id: "Wind", // @Model.Id
        title: "Rotor Speed", // @Model.Title
        label: "RPMs", // @Model.Label
        min: "0", // @Model.Min
        max: "50", // @Model.Max
        lowerActionLimit: 10,
        lowerWarningLimit: 15,
        upperWarningLimit: 40,
        upperActionLimit: 45,
        value: 20, // @Model.Value
        canvasBackColor: "#ffffff",
        minmaxColor: "#000000",
        gaugeBorderColor: "#8888cc",
        warningRangeColor: "#ffff00",
        showMinMax: true
    })

    var TempGuage = new GaugeSVG({
        id: "Nuke", // @Model.Id
        canvasBackColor: "#ffffff",
        min: "300", // @Model.Min
        max: "350", // @Model.Max
        minmaxColor: "#000000",
        title: "Operating Temp", // @Model.Title
        gaugeBorderColor: "#8888cc",
        label: "Degrees Celcuis", // @Model.Label
        warningRangeColor: "#ffff00",
        lowerActionLimit: 305,
        lowerWarningLimit: 310,
        upperWarningLimit: 335,
        upperActionLimit: 345,
        //gaugeBackColor: "#44ff44",
        value: 320, // @Model.Value
        showMinMax: true
    })

    var WaterGuage = new GaugeSVG({
        id: "Water", // @Model.Id
        title: "Water Level", // @Model.Title
        label: "Feet", // @Model.Label
        min: "-6", // @Model.Min
        max: "6", // @Model.Max
        lowerActionLimit: -5,
        lowerWarningLimit: -3,
        upperWarningLimit: 3,
        upperActionLimit: 5,
        value: 0.5, // @Model.Value
        canvasBackColor: "#ffffff",
        minmaxColor: "#000000",
        gaugeBorderColor: "#8888cc",
        warningRangeColor: "#ffff00",
        showMinMax: true
    })

    var xhttp_wind = new XMLHttpRequest();
    xhttp_wind.onload = function () {
        if (this.status == 200) {
            var wind = parseInt(this.responseText) / 10;
            WindGuage.refresh(wind, true);
            console.log(this.response);
        }
    };

    var xhttp_nuke = new XMLHttpRequest();
    xhttp_nuke.onload = function () {
        if (this.status === 200) {
            var nuke = parseInt(this.responseText) / 10;
            TempGuage.refresh(nuke, true);
            console.log(this.response);
        }
    };

    var xhttp_water = new XMLHttpRequest();
    xhttp_water.onload = function () {
        if (this.status === 200) {
            var water = parseFloat(this.responseText) / 10;
            WaterGuage.refresh(water, true);
            console.log(this.response);
        }
    };

    document.getElementById("water-post").onchange = function (e) {
        var request = new XMLHttpRequest();
        request.open("POST", "api/Hydro.php", true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.send("value="+e.target.value);
    };

    document.getElementById("nuke-post").onchange = function (e) {
        var request = new XMLHttpRequest();
        request.open("POST", "api/Nuclear.php", true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.send("value="+e.target.value);
    };

    document.getElementById("wind-post").onchange = function (e) {
        var request = new XMLHttpRequest();
        request.open("POST", "api/Wind.php", true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.send("value="+e.target.value);
    };

    setInterval(function () {
        xhttp_wind.open("GET", "wind.txt?"+new Date().getTime(), true);
        xhttp_wind.send();

        xhttp_nuke.open("GET", "nuke.txt?" + new Date().getTime(), true);
        xhttp_nuke.send();

        xhttp_water.open("GET", "water.txt?" + new Date().getTime(), true);
        xhttp_water.send();

	$( "#windspeed" ).load( "windspeed.txt?" + new Date().getTime() );

	$( "#coretemp" ).load( "temp.txt?" + new Date().getTime() );
	
	$( "#flow" ).load( "flow.txt?" + new Date().getTime() );

    }, 5000);
};
