<?php
/**
 * Created by PhpStorm.
 * User: MFiebig
 * Date: 3/10/2018
 * Time: 11:58 PM
 */

namespace Api\get_values;

require __DIR__."/Connections.php";
use Api\Connections;

$handle = Connections::getDbConnection();

$query = $handle->prepare(
        "SELECT value FROM OPC_Data WHERE station='resistance';");
$query->execute();
$resistance = $query->fetch();

$query = $handle->prepare(
        "SELECT value FROM OPC_Data WHERE station='coolant';");
$query->execute();
$coolant = $query->fetch();

$query = $handle->prepare(
        "SELECT value FROM OPC_Data WHERE station='spillway';");
$query->execute();
$spillway = $query->fetch();

?>
