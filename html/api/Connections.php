<?php
/**
 * Created by PhpStorm.
 * User: MFiebig
 * Date: 3/10/2018
 * Time: 11:59 PM
 */

namespace Api;

use PDO;

class Connections {

    private static $db_connection;

    private static $common_mysql_config = array(
        PDO::ATTR_EMULATE_PREPARES   => false,
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    );

    public static function getDbConnection()
    {
        if (!self::$db_connection) {
            //Connection info for the ESL database.
            $sqlServer = "historian.engineer.scadacs.us";
            $sqlUser = "opc_user";
            $sqlPass = "nbd84?P_fKtLWgz<";

            $db = 'scadacs_db';
            $dsn = "mysql:host=$sqlServer;dbname=$db";

            self::$db_connection = new PDO($dsn, $sqlUser, $sqlPass, self::$common_mysql_config);
        }

        return self::$db_connection;
    }
}
