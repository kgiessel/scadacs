<html>
    <head>
    	<title>Region 00 Scadacs Control System</title>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    	<script type="application/javascript" src="javascript/gaugeSVG.js"></script>
    	<script type="application/javascript" src="javascript/index.js"></script>
    	<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    	<link type="text/css" rel="stylesheet" href="index.css" />
    </head>
    <body>
	<div id="dial-container">
	    <div>
		<h1>Wind Turbine</h1>
	    	<div id="Wind"></div>
	    	<div>RPMs must be between 15 and 40<br></div>
	    	<div id="wrapper">
		    <div class="sensor">Wind Speed: </div>
		    <div id="windspeed" class="hmidata"></div>
		    <div class="label">MPH</div>
	    	</div>
	    	<div><br>
		    Adjust the resistance to compensate for wind speed<br><br>
		    Resistance (1 to 10)<br>
		    <?php include 'api/get_values.php'; ?>

		    <b>1</b> <input id="wind-post" type="range" min="1" max="10" value="<?php echo $resistance[0] ?>" list="tickmarks" /> <b>10</b>
  		</div>
	    </div>
      	    <div>
	    	<h1>Nuclear Reactor</h1>
	    	<div id="Nuke"></div>
	    	<div>Operating Temperature must be between 310 and 345 degrees</div>
	 	<div id="wrapper">
		    <div class="sensor">Core Temperature: </div>
		    <div id="coretemp" class="hmidata"></div>
		    <div class="label">Degrees</div>
		</div>
	    	<div><br>
		    Adjust the coolant flow to compensate for core temperature<br><br>
		    Coolant Flow (1 to 10)<br>
		    <b>1</b> <input id="nuke-post" type="range" min="1" max="10" value="<?php echo $coolant[0] ?>" list="tickmarks" /> <b>10</b>
		</div>
	    </div>
      	    <div>
	    	<h1>Hydro Electric Plant</h1>
	    	<div id="Water"></div>
            	<div>Water level must be within 3 feet above or below baseline</div>
		<div class="wrapper">
		    <div class="sensor">Water Flow: </div>
		    <div id="flow" class="hmidata"></div>
		    <div class="label">Feet</div>
		</div>
		<div><br>
		    Adjust the spillgate to compensate for water flow<br><br>
		    Spillgate (-5 to +5)<br>
		    <b>-5</b> <input id="water-post" type="range" min="-5" max="5" value="<?php echo $spillway[0] ?>" list="tickmarks2" /> <b>+5</b>
		</div>
	    </div>
    	</div>
        <datalist id="tickmarks">
          <option value="1" label="1">
          <option value="2">
          <option value="3">
          <option value="4">
          <option value="5" label="5">
          <option value="6">
          <option value="7">
          <option value="8">
          <option value="9">
          <option value="10" label="10">
	</datalist>
        <datalist id="tickmarks2">
	  <option value="-5" label="-5">
	  <option value="-4">
	  <option value="-3">
	  <option value="-2">
	  <option value="-1">
          <option value="0" label="0">
          <option value="1">
          <option value="2">
          <option value="3">
          <option value="4">
          <option value="5" label="5">
	</datalist>	  
  </body>
</html>
