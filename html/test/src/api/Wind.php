<?php
/**
 * Created by PhpStorm.
 * User: MFiebig
 * Date: 3/10/2018
 * Time: 11:58 PM
 */

namespace Api\Wind;

require __DIR__."/Connections.php";
use Api\Connections;

$value =  isset($_POST['value']) ? filter_var($_POST['value'], FILTER_SANITIZE_NUMBER_INT) : null;

if(!is_null($value)){
    $handle = Connections::getDbConnection();

    $query = $handle->prepare(
        "INSERT INTO OPC_Data (station, value)
        VALUE ('resistance', :value)
        ON DUPLICATE KEY UPDATE
        value=VALUES(value);");

    $query->execute(array(
        ":value" => $value
    ));
}