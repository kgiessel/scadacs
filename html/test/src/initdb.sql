CREATE DATABASE scadacs_db;

USE scadacs_db;

CREATE TABLE OPC_Data (
  station enum('resistance','coolant','spillway') NOT NULL PRIMARY KEY,
  value  smallint(5) NOT NULL
);

CREATE USER "opc_user" IDENTIFIED BY 'nbd84?P_fKtLWgz<';
GRANT UPDATE,INSERT,SELECT ON scadacs_db.OPC_Data TO "testy_testerson";