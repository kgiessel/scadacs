SCADACS Raspberry Pi Project
Code for the Raspberry Pi scada boards for PRCCDC 2018
https://giessel.net/raspberry-pi-scada/

Getting Started
Copy contents of html to /var/www/html
The rest of the code can go in /home/pi

Built With
Bash and Python with a bit of Jvascript for the website

Authors
Kurt Giessel kurt@giessel.net
