#!/usr/bin/env python

import RPi.GPIO as GPIO

GPIO.setwarnings(False)

GPIO.setmode(GPIO.BOARD)
GPIO.setup(11,GPIO.OUT)
GPIO.setup(13,GPIO.OUT)
GPIO.setup(15,GPIO.OUT)
GPIO.setup(29,GPIO.OUT)
GPIO.setup(31,GPIO.OUT)
GPIO.setup(33,GPIO.OUT)
GPIO.setup(37,GPIO.OUT)
GPIO.setup(36,GPIO.OUT)
GPIO.setup(32,GPIO.OUT)
GPIO.setup(22,GPIO.OUT)
GPIO.setup(18,GPIO.OUT)
GPIO.setup(16,GPIO.OUT)

GPIO.setup(35,GPIO.OUT)

GPIO.output(11,GPIO.LOW)
GPIO.output(13,GPIO.LOW)
GPIO.output(15,GPIO.LOW)
GPIO.output(29,GPIO.LOW)
GPIO.output(31,GPIO.LOW)
GPIO.output(33,GPIO.LOW)
GPIO.output(37,GPIO.LOW)
GPIO.output(36,GPIO.LOW)
GPIO.output(32,GPIO.LOW)
GPIO.output(22,GPIO.LOW)
GPIO.output(18,GPIO.LOW)
GPIO.output(16,GPIO.LOW)

GPIO.output(35,GPIO.HIGH)

#GPIO.cleanup()
