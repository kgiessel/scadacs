import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

GPIO.setup(40, GPIO.IN, pull_up_down=GPIO.PUD_UP)

while True:
    input_state = GPIO.input(40)
    if input_state == False:
        print('Button Pressed')
    time.sleep(0.2)
    print "%s" % input_state
