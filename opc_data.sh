#!/bin/bash

home='/home/pi/hmi'
host='sca-opc-p1.engineer.scadacs.us'
user='hmi_user'
password='nbd84?P_fKtLWgz<'
rsa="/home/pi/.ssh/id_rsa"

stations=( "resistance" "coolant" "spillway" )

#check if opc server is up

ftp_up=`nmap -Pn -p 21 $host | grep open`
if [ -z "$ftp_up" ]; then
  echo "5" > $home/resistance.txt
  echo "5" > $home/coolant.txt
  echo "0" > $home/spillway.txt
  echo "0" > /var/www/html/opc.status
else

echo "1" > /var/www/html/opc.status

for i in "${stations[@]}"
do

ftp -in $host <<EOF
user $user $password
get $i.txt
quit
EOF

mv /$i.txt $home/$i.txt
chown pi.pi $home/$i.txt

done

fi

scp -i $rsa blackteam@172.31.0.3:~/hmi/windspeed.txt $home/windspeed.txt
scp -i $rsa blackteam@172.31.0.3:~/hmi/temp.txt $home/temp.txt
scp -i $rsa blackteam@172.31.0.3:~/hmi/flow.txt $home/flow.txt

#reset to mid range if historian is down
resistance=`cat $home/resistance.txt`
if [ -z "$resistance" ]; then
  resistance="5"
fi
coolant=`cat $home/coolant.txt`
if [ -z "$coolant" ]; then
  coolant="5"
fi
spillway=`cat $home/spillway.txt`
if [ -z "$spillway" ]; then
  spillway="0"
fi
windspeed=`cat $home/windspeed.txt`
temp=`cat $home/temp.txt`
flow=`cat $home/flow.txt`

wind=`echo "scale=2;8.5 / $resistance * $windspeed * 10" | bc | cut -f1 -d"."`
nuke=`echo "($temp - 2 * $coolant) * 10" | bc | cut -f1 -d"."`
water=`echo "scale=2;($flow + $spillway) * 10" | bc | cut -f1 -d"."`

wind_status=`cat /home/scadacs/wind.status`
nuke_status=`cat /home/scadacs/nuke.status`
water_status=`cat /home/scadacs/water.status`
if [ $wind_status = "0" ]; then
  echo "" > /var/www/html/wind.txt
else
  echo "$wind" > /var/www/html/wind.txt
fi
if [ $nuke_status = "0" ]; then
  echo "" > /var/www/html/nuke.txt
else
  echo "$nuke" > /var/www/html/nuke.txt
fi
if [ $water_status = "0" ]; then
  echo "" > /var/www/html/water.txt
else
  echo "$water" > /var/www/html/water.txt
fi
